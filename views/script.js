/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
    $('#btn-send').on("click", function () {
        onBtnSendClick()
    })
    $("#btn-verify").on("click", function () {
        onBtnVerifyClick()
    });
    $('#userForm').on('submit', function (event) {
        event.preventDefault();
        handleFormSubmit();
    });
    $('#btn-check').on('click', function () {
        onBtnCheckClick();
    });
})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

function onPageLoading() {

}

function onBtnCheckClick() {
    const fullName = $('#inp-username').val().trim();
    const phone = $('#inp-number').val().trim();

    // Check if either field is missing and provide specific feedback
    if (!fullName) {
        alert('Please enter Username');
        return;
    }

    if (!phone) {
        alert('Please enter Phone Number');
        return;
    }

    // Construct URL for GET request
    let url = '/users/status?';
    if (fullName) {
        url += `fullname=${encodeURIComponent(fullName)}&`;
    }
    if (phone) {
        url += `phone=${encodeURIComponent(phone)}`;
    }

    // Send GET request
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        success: function (response) {
            if (response.status) {
                alert(`User status: ${response.status}`);
            }
        },
        error: function (error) {
            if (error.status === 404) {
                alert('User not found');
            } else {
                alert('Error checking user status');
                console.error(error);
            }
        }
    });
}
function onBtnSendClick() {
    const email = $('#inp-email').val();
    if (isValidEmail(email)) {
        const contactObj = {
            email: email
        };
        $.ajax({
            type: 'POST',
            url: '/contacts',
            data: JSON.stringify(contactObj),
            contentType: 'application/json',
            dataType: 'json',
            success: function (response) {
                alert('Contact created successfully!');
                console.log(response);
            },
            error: function (error) {
                alert('Error creating contact');
                console.log(error);
            }
        });
    }
}

function onBtnVerifyClick() {
    var vPhoneNumber = $("#inp-phone").val().trim();
    if (phonenumber(vPhoneNumber)) {
        alert("Invalid phone number")
    } else {
        alert("Valid phone number")
    }
}

function handleFormSubmit() {
    const fullName = $('#inp-name').val();
    const phone = $('#inp-phone').val();
    let isValid = true;
    let errorMessage = "";

    if (fullName.trim() === "") {
        isValid = false;
        errorMessage += "Name is required.\n";
    }
    if (phone.trim() === "") {
        isValid = false;
        errorMessage += "Phone is required.\n";
    }

    if (!isValid) {
        alert(errorMessage);
        return;
    }

    const formData = {
        fullName: fullName,
        phone: phone
    };

    $.ajax({
        type: 'POST',
        url: '/users',
        data: JSON.stringify(formData),
        contentType: 'application/json',
        dataType: 'json',
        success: function (response) {
            alert('User created successfully!');
            console.log(response);
        },
        error: function (error) {
            alert('Error submitting form');
            console.log(error);
        }
    });
}

/*** REGION 4 -  */

// hàm kiểm tra email
function isValidEmail(email) {
    // Định dạng pattern cho email
    const emailPattern = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;

    // Sử dụng test để kiểm tra định dạng
    return emailPattern.test(email);
}

// hàm kiểm tra số đt
function phonenumber(phone) {
    var phonePattern = /^\d{10}$/;
    return phonePattern.test(phone)
}