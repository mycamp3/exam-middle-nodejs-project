const express = require('express');
const app = express();
const path = require('path');
const port = 8000;
const mongoose = require('mongoose');
const userModel = require('./app/models/user.model');
//import model
const userRouter = require('./app/routes/user.router');
const contactRouter = require('./app/routes/contact.router');

//middleware
app.use(express.json());
app.use(express.static('views'));

app.use((req, res, next) => {
    const currentDate = new Date();
    console.log(`Current Date and Time: ${currentDate}`);
    next();
});

// Middleware để in URL của request ra console
app.use((req, res, next) => {
    console.log(`Requested URL: ${req.url}`);
    next();
});

// khai báo kết nối mongoDB qua mongoose
mongoose.connect("mongodb://127.0.0.1:27017/devcamp-vaccineng-backend")
    .then(() => {
        console.log("MongoDB connected");
    })
    .catch((err) => {
        console.log(err);
    });

// Routes
app.get('/', (req, res) => {
    res.send('Welcome to Vaccine.ng!');
});
// định nghĩa route cho trang chủ
app.get('/', (req, res) => {
    // đường dẫn đến file html trong thư mục
    const indexPath = path.join(__dirname, './views/index.html');
    res.sendFile(indexPath);
})

app.get('/admin/users', (req, res) => {
    // đường dẫn đến file html trong thư mục
    const indexPath = path.join(__dirname, './views/indexListUser.html');
    res.sendFile(indexPath);
})

app.get('/admin/contacts', (req, res) => {
    // đường dẫn đến file html trong thư mục
    const indexPath = path.join(__dirname, './views/indexListContact.html');
    res.sendFile(indexPath);
})

app.use('/users', userRouter);
app.use('/contacts', contactRouter);

app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
})