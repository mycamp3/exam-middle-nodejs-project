const mongoose = require('mongoose');
const { Schema } = mongoose;

// Define the User schema
const userSchema = new Schema({
  fullName: {
    type: String,
    required: true
  },
  phone: {
    type: String,
    required: true,
    unique: true
  },
  status: {
    type: String,
    enum: ["Level 0", "Level 1", "Level 2", "Level 3"],
    default: "Level 0"
  }
},{
  timestamps: true
});

// Create the User model
const User = mongoose.model('User', userSchema);

module.exports = User;