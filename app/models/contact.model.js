const mongoose = require('mongoose');
const { Schema } = mongoose;

// Define the Contact schema
const contactSchema = new Schema({
  email: {
    type: String,
    required: true
  }
},{
  timestamps: true
});

// Create the Contact model
const Contact = mongoose.model('Contact', contactSchema);

module.exports = Contact;