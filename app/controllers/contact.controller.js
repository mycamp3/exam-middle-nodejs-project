const mongoose = require('mongoose');
const contactModel = require('../models/contact.model');

const createContact = async (req, res) => {
    // B1: Thu thập dữ liệu
    const { email } = req.body;

    // B2: Validate dữ liệu
    if (!email) {
        return res.status(400).json({
            message: "Invalid email"
        });
    }

    // B3: Xử lý dữ liệu
    try {
        const newContact = new contactModel({ email });
        const result = await contactModel.create(newContact);

        return res.status(201).json({
            message: 'Contact created successfully!',
            data: result
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({
            message: 'Error creating contact'
        });
    }
};

const getAllContacts = async (req, res) => {
    try {
        const contacts = await contactModel.find();
        res.status(200).send({ message: 'Contacts retrieved successfully!', data: contacts });
    } catch (err) {
        console.error(err);
        res.status(500).send({ message: 'Error retrieving contacts' });
    }
};

const getContactById = async (req, res) => {
    // B1: Thu thập dữ liệu
    const contactid = req.params.contactid;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(contactid)) {
        return res.status(400).json({
            message: "Invalid contactid"
        });
    }

    try {
        const result = await contactModel.findById(contactid);

        if (!result) {
            return res.status(404).send({ message: 'Contact not found' });
        }

        res.status(200).send({ message: 'Contact retrieved successfully!', data: result });
    } catch (err) {
        console.error(err);
        res.status(500).send({ message: 'Error retrieving contact' });
    }
};

const updateContactById = async (req, res) => {
    // B1: Thu thập dữ liệu
    const contactid = req.params.contactid;
    const { email } = req.body;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(contactid)) {
        return res.status(400).json({
            message: "ContactID not valid"
        });
    }
    if (!email) {
        return res.status(400).json({
            message: "Invalid email"
        });
    }
    try {
        const contact = await contactModel.findByIdAndUpdate(contactid, { email }, { new: true });

        if (!contact) {
            return res.status(404).send({ message: 'Contact not found' });
        }

        res.status(200).send({ message: 'Contact updated successfully!', data: contact });
    } catch (err) {
        console.error(err);
        res.status(500).send({ message: 'Error updating contact' });
    }
};

const deleteContact = async (req, res) => {
    try {
        const contactid = req.params.contactid;

        await contactModel.findByIdAndDelete(contactid);

        res.status(200).send({ message: 'Contact deleted successfully!' });
    } catch (err) {
        console.error(err);
        res.status(500).send({ message: 'Error deleting contact' });
    }
};

module.exports = {
    createContact,
    getAllContacts,
    getContactById,
    updateContactById,
    deleteContact
};
