const mongoose = require('mongoose');
const userModel = require('../models/user.model');

const createUser = async (req, res) => {
    // B1: Thu thập dữ liệu
    const { fullName, phone, status } = req.body;

    // B2: Validate dữ liệu
    const validStatuses = ["Level 0", "Level 1", "Level 2", "Level 3"];
    if (!fullName || !phone) {
        return res.status(400).json({
            message: "Invalid fullName or phone"
        });
    }
    if (status && !validStatuses.includes(status)) {
        return res.status(400).json({
            message: "Invalid status"
        });
    }

    // B3: Xử lý dữ liệu
    try {
        const newUser = new userModel({ fullName, phone, status });
        const result = await userModel.create(newUser);

        return res.status(201).json({
            message: 'User created successfully!',
            data: result
        });
    } catch (err) {
        console.error(err);
        if (err.code === 11000) {  // Lỗi trùng lặp unique key
            return res.status(400).json({
                message: 'Phone number already exists'
            });
        }
        res.status(500).json({
            message: 'Error creating user'
        });
    }
};
const getAllUsers = async (req, res) => {
    try {
        const users = await userModel.find();
        res.status(200).send({ message: 'Users retrieved successfully!', data: users });
    } catch (err) {
        console.error(err);
        res.status(500).send({ message: 'Error retrieving users' });
    }
};
const getUserById = async (req, res) => {
    // B1: Thu thập dữ liệu
    const userid = req.params.userid;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userid)) {
        return res.status(400).json({
            message: "Invalid userid"
        });
    }

    try {
        const result = await userModel.findById(userid);

        if (!result) {
            return res.status(404).send({ message: 'User not found' });
        }

        res.status(200).send({ message: 'User retrieved successfully!', data: result });
    } catch (err) {
        console.error(err);
        res.status(500).send({ message: 'Error retrieving user' });
    }
};
const updateUserById = async (req, res) => {
    // B1: Thu thập dữ liệu
    const userid = req.params.userid;
    const { fullName, phone, status } = req.body;

    // B2: Validate dữ liệu
    const validStatuses = ["Level 0", "Level 1", "Level 2", "Level 3"];
    if (!mongoose.Types.ObjectId.isValid(userid)) {
        return res.status(400).json({
            message: "UserID not valid"
        });
    }
    if (!fullName || !phone) {
        return res.status(400).json({
            message: "Invalid fullName or phone"
        });
    }
    if (status && !validStatuses.includes(status)) {
        return res.status(400).json({
            message: "Invalid status"
        });
    }
    
    try {
        const user = await userModel.findByIdAndUpdate(userid, { fullName, phone, status }, { new: true });

        if (!user) {
            return res.status(404).send({ message: 'User not found' });
        }

        res.status(200).send({ message: 'User updated successfully!', data: user });
    } catch (err) {
        console.error(err);
        res.status(500).send({ message: 'Error updating user' });
    }
};

const deleteUser = async (req, res) => {
    try {
        const userid = req.params.userid;

        await userModel.findByIdAndDelete(userid);

        res.status(200).send({ message: 'User deleted successfully!' });
    } catch (err) {
        console.error(err);
        res.status(500).send({ message: 'Error deleting user' });
    }
};


module.exports = {
    createUser,
    getAllUsers,
    getUserById,
    updateUserById,
    deleteUser,
    
};
