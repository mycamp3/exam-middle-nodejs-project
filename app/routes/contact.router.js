const express = require('express');
const router = express.Router();

const contactController = require('../controllers/contact.controller');

// Define routes for Contact operations
router.post('/', contactController.createContact);
router.get('/', contactController.getAllContacts);
router.get('/:contactid', contactController.getContactById);
router.put('/:contactid', contactController.updateContactById);
router.delete('/:contactid', contactController.deleteContact);

module.exports = router;