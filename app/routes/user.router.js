const express = require('express');
const router = express.Router();

const userController = require('../controllers/user.controller');
const User = require('../models/user.model');

// Endpoint to check user status by username and phoneNumber
router.get('/status', async (req, res) => {
    let query = req.query;
    let fullName = query.fullName;
    let phone = query.phone;
    console.log(query);
    try {
        // Query MongoDB to find user by username and phoneNumber
        const user = await User.findOne({ $or: [{ fullName }, { phone }] });

        if (user) {
            // Return status of the user if found
            res.status(200).json({ status: user.status });
        } else {
            // User not found
            res.status(404).json({ message: 'User not found' });
        }
    } catch (error) {
        // Handle server error
        console.error(error);
        res.status(500).json({ message: 'Server error' });
    }
});
// Define routes for User operations
router.post('/', userController.createUser);
router.get('/', userController.getAllUsers);
router.get('/:userid', userController.getUserById);
router.put('/:userid', userController.updateUserById);
router.delete('/:userid', userController.deleteUser);


module.exports = router;